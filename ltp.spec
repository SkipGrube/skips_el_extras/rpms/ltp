# SPDX-License-Identifier: GPL-2.0-only
%define _unpackaged_files_terminate_build 1
%define _stripped_files_terminate_build 1
%define _build_id_links none

Name: ltp
Version: 20230929
Release: 1%{?dist}

Summary: Linux Test Project (LTP)
License: GPL-2.0-only
Group: Development/Tools
Url: https://linux-test-project.github.io/
Vcs: https://github.com/linux-test-project/ltp.git

#Requires: ltp-alt-lists
#Requires: ltp-testsuite = %EVR

Source0: https://github.com/linux-test-project/ltp/releases/download/%{version}/%{name}-full-%{version}.tar.xz
Source1: ltp

BuildRequires: pkgconfig(libmnl) 
BuildRequires: pkgconfig(openssl)
BuildRequires: keyutils-libs-devel 
BuildRequires: pkgconfig(libacl)
BuildRequires: libaio-devel
BuildRequires: pkgconfig(libcap)
BuildRequires: pkgconfig(libsctp)

BuildRequires: glibc-devel,glibc-headers,make,gcc,pkgconf,autoconf,automake,bison,flex,m4,kernel-headers,glibc-headers,tar


Requires: libmnl
Requires: openssl
Requires: keyutils-libs
Requires: libacl
Requires: libaio
Requires: libcap
Requires: lksctp-tools
Requires: bash

#%{?!_without_check:%{?!_disable_check:BuildRequires: /proc}}

%description
The Linux Test Project has a goal to deliver test suites to the
open source community that validate the reliability, robustness,
and stability of Linux.

The LTP testsuite contains a collection of tools for testing the
Linux kernel and related features. Our goal is to improve the Linux
kernel and system libraries by bringing test automation to the testing
effort. Interested open source contributors are encouraged to join.

Testing Linux, one syscall at a time (tm).

%prep
%autosetup -p1 -n %{name}-full-%{version}

%build
%configure --prefix=/opt/ltp
make all

%install
make install DESTDIR=${RPM_BUILD_ROOT}

mkdir -p ${RPM_BUILD_ROOT}/%{_bindir}
install -m 755 %SOURCE1  ${RPM_BUILD_ROOT}/%{_bindir}/ltp


# Fix:  make a relative symlink and not absolute (in the RPM buildroot)
pushd ${RPM_BUILD_ROOT}/opt/ltp
unlink runltp-ng
ln -s kirk runltp-ng
popd


%check
# todo:  add tests for the tests(?)

%files
/opt/ltp
/usr/bin/ltp
%doc /usr/share/man


%changelog
* Mon Jan 15 2024 Skip Grube <skip@rockylinux.org> 20230929-1
- Updated lksctp-tools requirement, for EL9 builds

* Mon Jan 15 2024 Skip Grube <skip@rockylinux.org> 20230929-0
- New LTP package for EL8, used old ALT package as a very general base
